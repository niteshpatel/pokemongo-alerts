import json
import time

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

alerts = []


def get(request):
    global alerts
    
    response = JsonResponse(alerts, safe=False)
    
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "*"
    
    return response

@csrf_exempt
def post(request): 
    global alerts
    
    alerts.append((time.time(), json.loads(request.body)))
    alerts = alerts[-100:]
    
    return HttpResponse()
