function setupTerminalTextWriter() {
    $(function () {
        var terminal = $('#terminal');
        terminal.typist({
            height: "100%"
        });
    });
}

function printHelp() {
    $(function () {
        $('#terminal')
            .typist('speed', 'fast')
            .typist('prompt').typist('type', 'usage: index.html#[comma-separated-list-of-options-and-pokedexIds]')
            .typist('prompt')
            .typist('prompt').typist('type', 'options:')
            .typist('prompt').typist('type', '--- notify: turn on Chrome desktop notifications')
            .typist('prompt').typist('type', '--- [pokedexIds]: do not show notifications for these pokemon')
            .typist('prompt').typist('type', '--- include: show ONLY the list of pokemon instead of excluding them')
            .typist('prompt').typist('type', '--- distance-50: only show pokemon within 50 metres')
            .typist('prompt').typist('type', '--- distance-100: only show pokemon within 100 metres')
            .typist('prompt').typist('type', '--- distance-150: only show pokemon within 150 metres')
            .typist('prompt').typist('type', '--- distance-200: only show pokemon within 200 metres')
            .typist('prompt')
            .typist('prompt').typist('type', 'example: ' + location.origin + '/static/index.html#16,96,distance-150,notify')
            .typist('prompt').typist('type', '... will not show Pidgey or Drowzee')
            .typist('prompt').typist('type', '... will only show Pokemon within 150 metres')
            .typist('prompt').typist('type', '... will trigger Chrome desktop notifications (if available)')
            .typist('prompt')
            .typist('prompt')
    });
}

function requestNotificationPermissions() {
    if (typeof Notification !== 'undefined') {
        Notification.requestPermission();
    }
}

function parseUserOptionsFromUrl() {
    window.options = document.location.hash.substring(1).split(',');
}

function setupGlobals() {
    window.here = {latitude: 51.5193687, longitude: -0.0785894};
    window.recentSightings = {};
}

function requestPokemonSightings() {
    jQuery.getJSON(location.origin + "/get", processPokemonSightings);
}

function processPokemonSightings(data) {
    var timestamp;
    var message;
    var distance;
    var disappearTime;
    var timeDelta;

    for (var i = 0; i < data.length; i++) {
        timestamp = data[i][0];

        if (timestamp > window.lastUpdate || 1) {
            window.lastUpdate = timestamp;
            message = data[i][1].message;

            //noinspection JSUnresolvedVariable
            if (!!window.recentSightings[message.encounter_id]) {
                continue;
            }

            distance = parseInt(haversine(window.here, {
                latitude: message.latitude,
                longitude: message.longitude
            }));

            //noinspection JSUnresolvedVariable
            console.log(message.pokemon_id + ": " + message.pokemon_name);

            //noinspection JSUnresolvedVariable
            var isPokemonInList = window.options.indexOf(String(message.pokemon_id)) > -1;
            if (window.options.indexOf("include") > -1 ? isPokemonInList : !isPokemonInList) {

                if (distance < getMaxSearchDistance()) {
                    //noinspection JSUnresolvedVariable
                    disappearTime = new Date((message.disappear_time + 3600) * 1000);
                    timeDelta = getTimeDelta(disappearTime);

                    if (!timeDelta.past) {
                        //noinspection JSUnresolvedVariable
                        recordInRecentSightings(message.encounter_id);

                        //noinspection JSUnresolvedVariable
                        outputSightingInfo({
                            id: message.pokemon_id,
                            name: message.pokemon_name,
                            location: message.latitude + "," + message.longitude,
                            distance: distance,
                            gone: timeDelta.minutes + " mins " + parseInt(timeDelta.seconds) + " secs "
                        })
                    }
                }
            }
        }
    }

    removeOldSightings();
}

function recordInRecentSightings(encounterId) {
    window.recentSightings[encounterId] = new Date();
}

function outputSightingInfo(pokemon) {
    var title = pokemon.name
        + " located " + pokemon.distance + " metres away!";

    var link = "<a target='_blank' href='https://www.google.com/maps/place/"
        + pokemon.location + "/@"
        + pokemon.location + ",17z" + "'>"
        + pokemon.distance + " metres away</a>";
    var body = " for " + pokemon.gone;
    var notificationBody = "Will be gone in " + pokemon.gone;

    $('#terminal')
        .typist('speed', 'slow')
        .typist('prompt')
        .typist('type', pokemon.name + " (" + pokemon.id + ") ")
        .typist('typeLink', link)
        .typist('type', body);

    window.scrollTo(0, 1000000);
    createDesktopNotification(title, notificationBody);
}

function removeOldSightings() {
    for (var key in window.recentSightings) {
        if (window.recentSightings.hasOwnProperty(key)) {
            if (new Date() - window.recentSightings[key] > 60 * 1000) {
                delete window.recentSightings[key];
            }
        }
    }
}

function createDesktopNotification(title, details) {
    if (window.options.indexOf("notify") == -1) {
        return;
    }

    if (typeof Notification == "undefined") {
        return;
    }
    if (Notification.permission != "granted") {
        return;
    }

    var notification = new Notification(title, {
        body: details,
        vibrate: true
    });

    notification.onclick = function () {
        window.focus()
    };
}

function getTimeDelta(futureDate) {
    var dateNow = new Date();

    // get total seconds between the times
    var rawDelta = futureDate - dateNow;
    var delta = Math.abs(rawDelta) / 1000;

    // calculate (and subtract) whole days
    var days = Math.floor(delta / 86400);
    delta -= days * 86400;

    // calculate (and subtract) whole hours
    var hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

    // calculate (and subtract) whole minutes
    var minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

    // what's left is seconds
    var seconds = delta % 60;  // in theory the modulus is not required

    return {
        past: rawDelta < 0,
        minutes: minutes,
        seconds: seconds
    };
}

function getMaxSearchDistance() {
    var distance = 1000;

    if (window.options.indexOf("distance-50") > -1) {
        distance = 50;
    }
    else if (window.options.indexOf("distance-100") > -1) {
        distance = 100;
    }
    else if (window.options.indexOf("distance-150") > -1) {
        distance = 150;
    }
    else if (window.options.indexOf("distance-200") > -1) {
        distance = 200;
    }

    return distance;
}
