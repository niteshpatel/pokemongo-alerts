Number.prototype.toRadians = function() {
   return this * Math.PI / 180;
};
 
var haversine = (function() {
  return function haversine(start, end) {
    var R = 6371e3; // metres
    var φ1 = start.latitude.toRadians();
    var φ2 = end.latitude.toRadians();
    var Δφ = (end.latitude-start.latitude).toRadians();
    var Δλ = (end.longitude-start.longitude).toRadians();
 
    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
 
    return R * c;
  }
 
})();
